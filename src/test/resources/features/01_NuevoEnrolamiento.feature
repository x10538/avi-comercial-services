@Sesion
Feature: Nuevo Enrolamiento Bienvenida
  @Enrolamiento
  Scenario: Flujo completo de Nuevo Enrolamiento Bienvenida
    Given el usuario hace el flujo de conversacion desde su "telefono"
    When acepta los terminos y condiciones
    And ingresa el tipo de documento "tipodocumento" y numero de documento "nrodocumento"
  #  Then confirma los datos ingresados anteriormente

    #And desenrolamos al cliente por servicios
   # Examples:
   #   | Descripcion         | telefono    | tipodocumento | nrodocumento |
#      |                     | 51987659213 | CE            | 318100001    |
#        | 1 TC - Match        | 51987659101 | DNI           | 18100045     |
#      |                     | 51952167874 | DNI           | 45497691     |
   #   | Mas de 1 TC - Match | 51966566088 | DNI           | 18100005     |
#      | No Match            | 51987659104 | DNI           | 18100045     |
#      | No Match            | 51987659151 | DNI           | 18100045     |
#      | No Match            | 51987659191 | DNI           | 18100045     |

  @Desenrolamiento
  Scenario: Desenrolamos al cliente por servicios
   And desenrolamos al cliente por servicios