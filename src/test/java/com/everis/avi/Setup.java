package com.everis.avi;

import com.everis.avi.util.Util;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.restassured.RestAssured;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

public class Setup {

    Util getConfigProperties = new Util();
    @Before
    public void setTheStage() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        Cast cast = OnlineCast.whereEveryoneCan(CallAnApi.at(getConfigProperties.getVariableSerenity("api_Env")));
        OnStage.setTheStage(cast);
    }

    @After
    public void closeTheStage() {
        OnStage.drawTheCurtain();
    }
}
