package com.everis.avi.rest.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Delete;

/**
 * @author YorkCorreaLaRosa on 21/12/2020
 * @project avi-automation-services
 */

public class Desenrolamiento implements Task {

    private final String telefono;

    public Desenrolamiento(String telefono) {

        this.telefono = telefono;
    }

    public static Performable withTestDesenrolamiento(String telefono) {
        return Tasks.instrumented(Desenrolamiento.class, telefono);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Delete.from("/enrollment/EXT_CONTACT?phoneNumber=" + telefono));
    }
}
