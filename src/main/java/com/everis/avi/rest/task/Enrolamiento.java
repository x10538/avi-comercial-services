package com.everis.avi.rest.task;

import com.everis.avi.util.Util;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

/**
 * @author YorkCorreaLaRosa on 21/12/2020
 * @project avi-automation-services
 */

public class Enrolamiento implements Task {

    Util getConfigProperties = new Util();
    private static final String TEMPLATE_DIALOGO = "/templates/dialogo.json";
    private final String textoDilogoAvi;
    private final String codigoSesion;
    private final String telefono;

    public Enrolamiento(String textoDilogoAvi, String codigoSesion, String telefono) {
        this.textoDilogoAvi = textoDilogoAvi;
        this.codigoSesion = codigoSesion;
        this.telefono = telefono;
    }

    public static Performable withTestEnrolamiento(String textoDilogoAvi, String codigoSesion, String telefono) {
        return Tasks.instrumented(Enrolamiento.class, textoDilogoAvi, codigoSesion, telefono);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(codigoSesion).with(request -> request
                .contentType(ContentType.JSON)
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .header("API-KEY", "20dcb8cb-4603-4efa-a78a-8bb4f83ce46a")
                .header("PROJECT", "EXT_COMERCIAL")
                .header("CHANNEL", "WAPP_COM")
                .header("OS", "android")
                .header("USER-REF", telefono)
                .header("LOCALE", "es-ES")
                .header("OS-VERSION", "10")
                .header("BROWSER", "Chrome")
                .header("BROWSER-VERSION", "10")
                .header("BUSINESS-KEY", "123")
                .header("Ocp-Apim-Subscription-Key", getConfigProperties.getVariableSerenity("key_Env"))
                .body(Util.getTemplate(TEMPLATE_DIALOGO)
                        .replace("{text}", textoDilogoAvi))));
    }
}
