package com.everis.avi.rest.steps;

import com.everis.avi.rest.fact.Access;
import com.everis.avi.rest.task.Enrolamiento;
import com.everis.avi.util.Util;
import io.cucumber.java.en.*;
import net.serenitybdd.rest.SerenityRest;
import org.apache.http.HttpStatus;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.*;

public class ConsultaSaldoTcStepDefinition {

    Util getConfigProperties = new Util();

    private String cantidadTarjetas;
    String Telefono;
    String codigoSesion = "";
    @Given("el usuario inicia el flujo de conversacional desde su {string}")
    public void elUsuarioIniciaElFlujoDeConversacionalDesdeSu(String nroTelefono) {
        theActorCalled("Test").has(Access.toAvi());
        Telefono = getConfigProperties.getVariableSerenity("var_numero");
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("Hola", codigoSesion, Telefono));
        codigoSesion = SerenityRest.lastResponse().path("sessionCode");
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()));
    }

    @When("hace la consulta de Saldo de TC")
    public void haceLaConsultaDeSaldoDeTC() {
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("Quiero saber el saldo de mi tc", codigoSesion, Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()));
    }

    @And("se verificara si el usuario cuenta con una o más tarjetas")
    public void seVerificaraSiElUsuarioCuentaConUnaOMásTarjetas() {
        cantidadTarjetas = SerenityRest.lastResponse().path("answers.code[0]").toString().replaceAll("\\[", "").replaceAll("]", "");
    }

    @And("si cuenta con una tarjeta verificara su saldo sino escogera una tarjeta de la lista {string}")
    public void siCuentaConUnaTarjetaVerificaraSuSaldoSinoEscogeraUnaTarjetaDeLaLista(String consultaTarjeta) {
        if (cantidadTarjetas.equalsIgnoreCase("TRX_CARD_BALANCE")) {
            theActorInTheSpotlight().should(seeThatResponse(response ->
                    response.statusCode(HttpStatus.SC_OK).log().all()
                            .body("answers.code", hasItem("SALD_028")))
            );

        } else if (cantidadTarjetas.equalsIgnoreCase("TRX_CARD_LIST")) {
            theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento(consultaTarjeta, codigoSesion, Telefono));
            theActorInTheSpotlight().should(seeThatResponse(response ->
                    response.statusCode(HttpStatus.SC_OK).log().all()
                            .body("answers.code", hasItem("TRX_CARD_BALANCE"))
                            .body("answers.code", hasItem("SALD_028")))
            );
        }
    }

    @Then("por ultimo salimos para poder cerrar el ticket")
    public void porUltimoSalimosParaPoderCerrarElTicket() {
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("salir", codigoSesion, Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()
                        .body("answers.code", hasItem("DESP_013"))
                  //      .body("answers.code", hasItem("DESP_012"))
                 //       .body("answers.title", hasItem("CLOSE_TICKET"))
                )
        );
    }

}
