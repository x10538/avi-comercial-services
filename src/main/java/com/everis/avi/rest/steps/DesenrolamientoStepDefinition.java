package com.everis.avi.rest.steps;

import com.everis.avi.rest.task.Desenrolamiento;
import com.everis.avi.util.Util;
import io.cucumber.java.en.And;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import org.apache.http.HttpStatus;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class DesenrolamientoStepDefinition {

    Util getConfigProperties = new Util();
    String Telefono;

    @And("desenrolamos al cliente por servicios")
    public void desenrolamosAlClientePorServicios() throws InterruptedException {

        theActorCalled("sam").whoCan(CallAnApi.at(getConfigProperties.getVariableSerenity("api_unroll")));
        Telefono = getConfigProperties.getVariableSerenity("var_numero");
        theActorInTheSpotlight().attemptsTo(Desenrolamiento.withTestDesenrolamiento(Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
               response.statusCode(HttpStatus.SC_OK).log().all()));


    }
}
