package com.everis.avi.rest.steps;

import com.everis.avi.rest.fact.Access;
import com.everis.avi.rest.task.Enrolamiento;
import com.everis.avi.util.Util;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import org.apache.http.HttpStatus;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.hasItem;

public class ConsultaCreditosCyTStepDefinition {

    Util getConfigProperties = new Util();
    String Telefono;
    String codigoSesion = "";

    @Given("el usuario inicia el flujo conversacional con Avi desde el numero de telefono")
    public void elUsuarioIniciaElFlujoConversacionalConAviConNumeroDeTelefono() {
        theActorCalled("Test").has(Access.toAvi());
        Telefono = getConfigProperties.getVariableSerenity("var_numero");
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("Hola", codigoSesion, Telefono));

        codigoSesion = SerenityRest.lastResponse().path("sessionCode");
        //guardo ultimo codigo de sesion
        Serenity.setSessionVariable("codigoSesion_var").to(codigoSesion);
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()));
    }

    @And("selecciona opcion cuentas creditos y tarjetas")
    public void seleccionaOpcionCreditosCuentasYTarjetas() {
        //Obtengo ultimo codigo de sesion
        codigoSesion = Serenity.sessionVariableCalled("codigoSesion_var");

        Telefono = getConfigProperties.getVariableSerenity("var_numero");
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("A", codigoSesion, Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()
                        .body("answers.code", hasItem("MENU_003")))

        );
    }

    @And("selecciona opcion cuenta negocio")
    public void seleccionaOpcionCuentaNegocio() {
        //Obtengo ultimo codigo de sesion
        codigoSesion = Serenity.sessionVariableCalled("codigoSesion_var");
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("A", codigoSesion, Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                        response.statusCode(HttpStatus.SC_OK).log().all()));

        String validacion = SerenityRest.lastResponse().path("context.caseResultBA").toString().replaceAll("\\[", "").replaceAll("]", "");

        System.out.println("VALIDACION:" + validacion);
        if(validacion.equals("3.0")) {
            //SOLO UNA CUENTA DOC OK TELEFONO NO COINCIDE
            System.out.println("se ingresa a validacion 3:");
            //CLIENTE
            Telefono = getConfigProperties.getVariableSerenity("var_numero");
            theActorInTheSpotlight().should(seeThatResponse(response ->
                            response.statusCode(HttpStatus.SC_OK).log().all()
                                    .body("answers.code", hasItem("CNEG_001"))
                                    .body("answers.code", hasItem("MENU_007"))

                    )
            );
        }
    }
}
