package com.everis.avi.rest.steps;

import com.everis.avi.rest.fact.Access;
import com.everis.avi.rest.question.Mensaje;
import com.everis.avi.rest.task.Enrolamiento;
import com.everis.avi.util.Util;
import io.cucumber.java.en.*;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import org.apache.http.HttpStatus;
import java.util.concurrent.TimeUnit;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.*;

public class ConsultaPyRStepDefinition {
    Util getConfigProperties = new Util();
    String Telefono;
    String codigoSesion = "";

    @Given("el usuario inicia el flujo conversacional con Avi con numero de telefono {string}")
    public void elUsuarioIniciaElFlujoConversacionalConAviConNumeroDeTelefono(String nroTelefono) {
        theActorCalled("Test").has(Access.toAvi());
        Telefono = getConfigProperties.getVariableSerenity("var_numero");
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("Hola", codigoSesion, Telefono));
        codigoSesion = SerenityRest.lastResponse().path("sessionCode");
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()));
    }

    @When("hacemos la consulta para saber el estado de mi reclamo")
    public void hacemosLaConsultaParaSaberElEstadoDeMiReclamo() {
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento("Cual es el estado de mi reclamo", codigoSesion,Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all()
                       // .body("answers.code", hasItem("PERE_008"))
                        //Se obtiene esta respuesta ya que no tiene reclamos
                        .body("answers.code", hasItem("PERE_077"))
                        .body("answers.code", hasItem("PERE_002"))
                        .body("answers.code", hasItem("PERE_002"))
        ));
    }

    @And("ingresamos el número de reclamo {string}")
    public void ingresamosElNúmeroDeReclamo(String nroReclamo) {
        theActorInTheSpotlight().attemptsTo(Enrolamiento.withTestEnrolamiento(nroReclamo, codigoSesion,Telefono));
        theActorInTheSpotlight().should(seeThatResponse(response ->
                response.statusCode(HttpStatus.SC_OK).log().all().contentType(ContentType.JSON)
                        //.body("context.result", not(isEmptyString()))
                        .time(lessThanOrEqualTo(5000L), TimeUnit.MILLISECONDS)
        ));
    }

    @Then("verificamos el codigo de respuesta {string} para el tipo de reclamo ingresado")
    public void verificamosElCodigoDeRespuestaParaElTipoDeReclamoIngresado(String codRespuesta) {
        theActorInTheSpotlight().should(seeThat("Codigo de Respuesta", Mensaje.codigoRespuesta(), equalToIgnoringCase(codRespuesta)));
    }
}