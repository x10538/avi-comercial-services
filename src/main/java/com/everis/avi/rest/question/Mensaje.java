package com.everis.avi.rest.question;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Question;

public class Mensaje {

    public static Question<String> codigoRespuesta() {
        return actor -> SerenityRest.lastResponse().path("answers.code").toString().replaceAll("\\[", "").replaceAll("]", "");
    }

    public static Question<String> resultadoContexto() {
        return actor -> SerenityRest.lastResponse().path("context.result").toString();
    }

    public static Question<String> resultadoTarjeta() {
        return actor -> SerenityRest.lastResponse().path("context.card.result").toString();
    }

}