package com.everis.avi.util;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Util {
    EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();

    public String getVariableSerenity(String tipoVariable) {
        return EnvironmentSpecificConfiguration.from(variables).getProperty(tipoVariable);
    }

    public static String getTemplate(String templatePath) {
        try {
            return IOUtils.toString(new ClassPathResource(templatePath).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
